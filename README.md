# Disclaimer
While this is against the End License Agreement since you are installing on non-Apple hardware, it's technically legal under the DMCA exemptions for anti-circumvention provisions for noncommercial use. However, you need to have a Mac to legally download a copy of macOS Mojave. All other methods are considered piracy and we do not condone it. 

# About This Build
![](https://i.imgur.com/WtcJXTw.jpg)
![](https://i.imgur.com/RVXxucd.jpg)

After the disappointing keynote in October of last year and still no announcement for a new Mac Pro, I thought of building a Hackintosh, mainly on an Intel X99 platform. After looking on eBay, I found a used combo with a Core i7 5820k, an ASUS Sabertooth motherboard and 16 GB of DDR4 memory for $512. Considering that x99 motherboards, 16 GB DDR4 memory and a 6800k processor will cost more than the price the seller is selling for and a Skylake combo will cost a bit more, it's hard to pass up this deal.

However, X99 builds are difficult to set up since Apple never made a Mac Pro using the X99 platform. Compared to other X99 boards, the Sabertooth X99 works mostly thanks to [kgp's work](https://www.tonymacx86.com/threads/imac-pro-x99-live-the-future-now-with-macos-10-14-mojave-successful-build-extended-guide.255117/).  Thanks to his work, Moave and now Catalina runs smoothly on a X99 Sabertooth.

I have since upgrade to a MacBook Pro 2018, which almost have the processing power of a 5820k. However, I still maintain this Hackintosh for more intensive tasks. I plan to save up and upgrade to an actual 2019 Mac Pro in the future.

This guide has been updated to support macOS Catalina. The EC patches that are required for the system to boot is now added. This guide will also support macOS Mojave.

## Parts

Type|Item|Price
:----|:----|:----
**CPU** | [Intel - Core i7-5820K 3.3 GHz 6-Core Processor](https://pcpartpicker.com/product/6tXfrH/intel-cpu-bx80648i75820k) | Purchased For $300.00 
**CPU Cooler** | [Noctua - NH-D15 82.5 CFM CPU Cooler](https://pcpartpicker.com/product/4vzv6h/noctua-cpu-cooler-nhd15) | Purchased For $87.95 
**Motherboard** | [Asus - SABERTOOTH X99 ATX LGA2011-3 Motherboard](https://pcpartpicker.com/product/yxYWGX/asus-motherboard-sabertoothx99) | Purchased For $200.00 
**Memory** | [Corsair - Vengeance LPX 16 GB (4 x 4 GB) DDR4-2666 Memory](https://pcpartpicker.com/product/74DzK8/corsair-memory-cmk16gx4m4a2666c16r) | Purchased For $0.00 
**Memory** | [Corsair - Vengeance LED 16 GB (2 x 8 GB) DDR4-3000 Memory](https://pcpartpicker.com/product/J7RFf7/corsair-vengeance-led-16gb-2-x-8gb-ddr4-3000-memory-cmu16gx4m2c3000c15r) | Purchased For $169.99 
**Storage** | [Corsair Force Series MP510 480GB NVMe PCIe Gen3 x4 M.2 SSD](https://pcpartpicker.com/product/nGzkcf/corsair-mp510-480gb-m2-2280-solid-state-drive-cssd-f480gbmp510) | Purchased For $64.99 
**Storage** | [Crucial - MX300 275 GB 2.5" Solid State Drive](https://pcpartpicker.com/product/rrvZxr/crucial-mx300-275gb-25-solid-state-drive-ct275mx300ssd1) | Purchased For $89.99 
**Storage** | [Seagate - FireCuda 1 TB 2.5" 5400RPM Hybrid Internal Hard Drive](https://pcpartpicker.com/product/w6x9TW/seagate-firecuda-1tb-25-5400rpm-hybrid-internal-hard-drive-st1000lx015) | Purchased For $59.99 
**Storage** | [Western Digital - Red 3 TB 3.5" 5400RPM Internal Hard Drive](https://pcpartpicker.com/product/7sTmP6/western-digital-internal-hard-drive-wd30efrx) | Purchased For $99.99 
**Storage** | [Western Digital - Red 3 TB 3.5" 5400RPM Internal Hard Drive](https://pcpartpicker.com/product/7sTmP6/western-digital-internal-hard-drive-wd30efrx) | Purchased For $105.99 
**Video Card** | [PowerColor - Radeon RX VEGA 56 8 GB Video Card](https://pcpartpicker.com/product/NjtWGX/powercolor-radeon-rx-vega-56-8gb-video-card-axrx-vega-56-8gbhbm2-3dh) | Purchased For $385.00 
**Case** | [Fractal Design - Define R6 ATX Mid Tower Case](https://pcpartpicker.com/product/n297YJ/fractal-design-define-r6-black-tg-atx-mid-tower-case-fd-ca-def-r6-bk-tg) | Purchased For $72.25 
**Power Supply** | [EVGA - 750 W 80+ Bronze Certified Semi-modular ATX Power Supply](https://pcpartpicker.com/product/JYyFf7/evga-power-supply-110b20750vr) | Purchased For $79.48 
**Case Fan** | [Noctua - NF-A14 FLX 68 CFM 140 mm Fan](https://pcpartpicker.com/product/Q3Lypg/noctua-case-fan-nfa14flx) | Purchased For $21.47 
**Case Fan** | [Noctua - NF-A14 FLX 68 CFM 140 mm Fan](https://pcpartpicker.com/product/Q3Lypg/noctua-case-fan-nfa14flx) | Purchased For $21.95 
**Case Fan** | [Noctua - NF-P14s redux-1500 PWM 78.69 CFM 140 mm Fan](https://pcpartpicker.com/product/8r9KHx/noctua-case-fan-nfp14sredux1500pwm) | Purchased For $12.02 
**Monitor** | [Dell - U2415 24.1" 1920x1200 60 Hz Monitor](https://pcpartpicker.com/product/zwPzK8/dell-monitor-u2415) | Purchased For $187.00 
**Monitor** | [Dell - U2415 24.1" 1920x1200 60 Hz Monitor](https://pcpartpicker.com/product/zwPzK8/dell-monitor-u2415) | Purchased For $224.95 
**Keyboard** | [Corsair - STRAFE Wired Gaming Keyboard](https://pcpartpicker.com/product/dJbkcf/corsair-keyboard-ch9000088na) | Purchased For $69.99 
**Wireless** | [BCM94360CD / BCM94331CD to PCI-E PCI Express x1 Adapter](https://pcpartpicker.com/product/3yFXsY/bcm94360cd-bcm94331cd-to-pci-e-pci-express-x1-adapter) | Purchased For $20.99 
**Wireless** | [Apple Broadcom BCM94331CD](http://www.osxwifi.com/product/apple-broadcom-bcm94331cd-802-11-a-b-g-n-with-bluetooth-4-0/r) | Purchased For $34.99 
**Mouse** | [Logitech MX Anywhere 2S Wireless Mouse with FLOW Cross-Computer Control and File Sharing for PC and Mac - 910-005132](https://pcpartpicker.com/product/BpTrxr/logitech-mx-anywhere-2s-wireless-mouse-with-flow-cross-computer-control-and-file-sharing-for-pc-and-mac-910-005132) | Purchased For $56.95 
| *Prices include shipping, taxes, rebates, and discounts* |
| **Total** | **$2365.93**


# Building the System

While I have worked with PC hardware such as upgrading RAM, adding a new HD, upgrading processors, it was easy. The Fractal Design R6 case is easy to work with. I simply installed the power supply, mounted the hard drives and SSDs to the hard drive trays and install the standoffs. 

You should install your CPU, heat sink and memory before putting it into the case. Use the motherboard box to place your mother board on or an anti-static mat. The Noctua NH-D15 comes with NT-H1, which works great and possibly better than Artic Silver. For 2011v2 systems, you should use the line method. Do not put thermal paste on the socket, only on the installed CPU.

Installing the heatsink was easy. I went with the Noctua NH-D15 dual tower cooler since I like how quiet the fans are despite the ugly coloring and it performs almost like a 2 radiator all in one water cooler. Installing it is easy. Simply install the bolts around the processor, place the brackets, screw them down, apply compound and install the heatsink.

After, assembling the system. I connected all the power connectors, PCIe cards including graphics cards, hard drives and fans together. I got the front panel connectors right on the first try as well.

As for wireless, I decided to order an official Apple BCM94331CD since I don't necessarily need Wireless AC and the computer will be hooked to the Ethernet eventually.

Lastly, for graphics, I opted for a Vega 56. Any Vega card is supported under macOS Mojave, including the new AMD Radeon VII under 10.4.5 and work well with encoding and decoding H264 video files. Nvidia cards are not recommended due to buggy drivers in High Sierra and the lack of macOS Mojave drivers. 

**Note:** If you have a reference Vega 56/64 or Frontier card, you will need Whatevergreen.kext as only one DisplayPort will output video. For other cards with more than 4 ports, you do not need Whatevergreen.kext and you can remove it from the /KEXTS/Other folder in the Clover folder.

As for RX 4xx/5xx series cards, they will work, but encoding/decoding videos will not work. This means playback of Amazon/Netflix in Safari won't work and encoding video with Final Cut Pro X will cause the system to freeze. This is because it expects Intel UHD graphics that are found on the iMac.

# BIOS Settings

First off, you need to flash the BIOS to use xcpm patches with the new method. This is required for future versions of macOS and also increases compatibility. This makes things a lot easier in the long run. Note that all this work is already done and you only need to put the included BIOS image to the flash drive.

1. Unzip and copy the **X99ST.CAP **  to a FAT-formatted USB2.0 storage device.

2. Shut-down your computer connect the USB2.0 storage device to the USB-port marked Flashback with a white outline. Press the BIOS-Flashback button for three seconds until the flashback-led starts to blink, indicating that the BIOS Flashback is in progress. Release the button.  If it stays solid, the flash was not successful since it cannot find the BIOS file. If that is the case, make sure the flash drive is formatted FAT32 and try again.\
3. Wait until the Flashback-led stops blinking and turns off, indicating that the BIOS Flashback was successful.

4. Boot your system and apply the BIOS settings as shown below.

## AI Tweaker:

* ASUS Multicore Enhancement -- Disabled

* Internal PPL Overvoltage -- Disabled

Note: If you are overclocking, my settings are below, but it depends on how good or bad your CPU is.

* AI Overclock Tuner: XMP

* CPU Core Ratio: Sync All Cores

* Core Ratio: 42 (depending on your CPU quality)

* Manual Voltage: 1.35v (depending on your CPU quality)

## Advanced

* Intel (VMX) Virtualization Technology: Enabled

### CPU Configuration > Power Management Configuration

* Enhanced Intel SpeedStep Technology -- Disabled

* Turbo mode: Enabled

* CPU C-State: Enabled

### USB Configuration

* Intel xHCI Mode: Enabled

* EHCI Legacy Support: Enabled

* xHCI Hand-off: Enabled

* EHCI Hand-off -- Enabled

### Onboard Devices Configuration:

* SPDIF Out Type: HDMI

## Boot

* Fast Boot: Disabled

* Boot Logo Display: Full Screen

* NumLock State: Disabled

* Above 4G Decoding: Enabled

* Setup Mode: Advanced

### CSM (Compatibility Support Module) 

* Launch CSM: Disabled

### Secure Boot

* OS Type: Other OS

## Other Considerations
If you are using a Broadwell-E CPU, you need to change the config.plist values. See the readme of the orginal X99 EFI distribution [here](https://github.com/KGP/X99-EFI-Folder-Distributions/blob/master/README.md).

Using a CPU with more than 6 cores? In the Clover > kext > Other folder, right click on TSCAdjustReset.kext > Contents and open config.plist. Look for the IOCPUNumber key. The value is (# of cores * 2)-1. If you have six cores, you multiply 6 by 2, which will gives you 12. Subtract the number by 1 and you will get 11.
![](https://i.imgur.com/zg75PsQ.png)

# Installation

The installation setup assumes you have the same basic hardware: SATA SSD, Vega 56, Core i7 5820k.

1. On a real Mac, download the macOS Mojave installer from the Mac App Store. Format the drive you will use as the Mojave/Catalina installer and name it "USB"
2. Create the Mojave/Catalina Installer to an USB flash drive or hard drive:

## Mojave

```sudo /Applications/Install\ macOS\ Mojave.app/Contents/Resources/createinstallmedia --volume /Volumes/USB --applicationpath /Applications/Install\ macOS\ Mojave.app --nointeraction```

## Catalina
```sudo /Applications/Install\ macOS\ Catalina.app/Contents/Resources/createinstallmedia --volume /Volumes/USB --applicationpath /Applications/Install\ macOS\ Catalina.app --nointeraction```

3. Use the EFI Mounter to mount the EFI partition. Copy Boot and Clover folder from the EFI folder distribution zip file to the EFI folder on the Mojave Installer partition.
![](https://i.imgur.com/Zm6dUDy.png)
![](https://i.imgur.com/EMDiC0K.png)
![](https://i.imgur.com/DLJDSnM.png)
4. Copy Copy [Clover Configurator](https://www.tonymacx86.com/resources/clover-configurator.429/) and [EFI Mounter v3](https://www.tonymacx86.com/resources/efi-mounter-v3.280/) to the Mojave installer. We will need these for the post install.
5. Reboot the computer and select the Clover macOS Mojave/Catalina installer.
6. When the clover menu appears, select the macOS Mojave/Catalina installer. Follow the install process
7. Once installation is complete, copy Clover Configurator to the Applications folder. Use Clover Configurator to mount both the macOS Mojave/Catalina installer drive and also the drive you installed Mojave/Catalina on. To do this, select the Mount EFI section and click "Mount Partition" to mount the EFI partition for the given drive.
8. Open the Mojave Installer EFI folder and the EFI folder on the drive you installed Mojave/Catalina on. Copy the Boot and Clover folders to the EFI folder of the EFI partition where you installed Mojave on. You can use Clover Configurator to mouint both EFI partitions of your Mojave installer and the disk you installed Mojave on.
![](https://i.imgur.com/MpWvSyZ.png)
![](https://i.imgur.com/UYXvLGB.png)
9. Open Clover Configurator. We will generate a serial number to make iMessage work. Go to the Mount EFI section and unmount the Mojave/Catalina Installer EFI partition.
![](https://i.imgur.com/hPkRDAE.png)
10. Click Open Partition of the drive that contains your Mojave/Catalina installation. Open the EFI and then the Clover folder. Drag the config.plist to Clover Configurator or open it by clicking File > Open
![](https://i.imgur.com/4dO8qIi.png)
11. Click the SMBIOS to view the settings for the SMBIOS. This contains information about the computer, including the serial number, board serial number and UUID. First, click Generate New under Serial Number. Then click "Check Coverage" to ensure the serial number is invalid and not taken by an actual iMac Pro. If you get "We're sorry, but this serial number isn't valid. Please check your information and try again," you are good. If you see warranty information, generate a new serial number and repeat the process until you receive this message.
12. Next, generate a new SMUUID by clicking Generate New button under SMUUID.
![](https://i.imgur.com/dIgy9bD.png)
13. Save the Config.plist by clicking File > Save or CMD+S. Close Clover Configurator.
14. Eject the Mojave installer and unplug it from your system. Reboot the system. If you see the Clover Menu without the installer, you are all set.

# Current Status
## What Works
* Audio including the front panel
* USB 3.0
* USB 2.0
* USB 3.1
* iMessage
* Graphics (with Whatevergreen.kext installed for 4 port Vega cards)
* Second Ethernet Port 
* Wireless (With Apple BCM94331CD and an adapter.)
* Sleep (For the most part)
* NVME (disables the last x16 PCIe slot if using a 5820k/6800k)

## Does Not Work
* CPU and other temperture sensors

# Like this Guide?
You are always free to donate via [Ko-fi](http://ko-fi.com/J3J6RY6D).
